cmake_minimum_required(VERSION 3.10)

project(SeddiTest VERSION 1.0)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED True)

add_executable( SeddiTest1 code/results-12.cpp )
add_executable( SeddiTest2 code/results-3.cpp )