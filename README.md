# Seddi-Test
This project contains all the generated documents and code solutions created by Sergio Emilio Galindo Ruedas.

The respository is structured as follows:

* The folder /doc contains the original .ODT file with the solutions and explanations
 as well as its resulting PDF. 
* The folder /diagrams contains the original .graphml diagram files and their 
resulting PDF files. 
* The folder /code contains all the code files created for this test.

## Building

In order to build the executables related to this project, please follow these steps:

```bash
git clone https://sergiogalindo@bitbucket.org/sergiogalindo/seddi-test.git
mkdir seddi-test/build && cd seddi-test/build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```
