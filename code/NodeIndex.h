#include <stdint.h>
#include <algorithm>
#include <type_traits>
#include <iostream>

struct NodeIndex
{
  uint32_t x;
	uint32_t y;
	uint32_t z;

	NodeIndex() {}
	
	NodeIndex( uint32_t i_x, uint32_t i_y, uint32_t i_z)
	: x(i_x)
	, y(i_y)
	, z(i_z)
  { }

	~NodeIndex() {}

	NodeIndex( const NodeIndex &other)
	// Question 1 code
	// Comment the following lines to observe the results
	: x( other.x )
	, y( other.y )
	, z( other.z )
	{ }

	NodeIndex(NodeIndex &&other)  noexcept
	{
	  // Question 1 code
    std::swap( x, other.x );
    std::swap( y, other.y );
    std::swap( z, other.z );
	}

	NodeIndex & operator =( const NodeIndex &other)
	{
		x = other.x;
		y = other.y;
		z = other.z;
		return * this;
	}

	NodeIndex & operator =(NodeIndex &&other) noexcept
	{
	  std::swap(x, other.x);
		std::swap(y, other.y);
		std::swap(z, other.z);
		return * this ;
	}

	void print( void ) const
	{
	  std::cout << this << " "
	            << x << ", " << y << ", " << z
	            << std::endl;
	}

};

static  bool  operator ==( const NodeIndex &p1,  const NodeIndex &p2)
{
return p1.x == p2.x && p1.y == p2.y && p1.z == p2.z;
}


// *****************
//  Question 2 code

// Define available types
// Comment lines to test
template < typename T > struct supported_types { enum { value = false }; };
template <> struct supported_types< uint32_t > { enum { value = true }; };
template <> struct supported_types< int32_t > { enum { value = true }; };
template <> struct supported_types< float > { enum { value = true }; };
template <> struct supported_types< double > { enum { value = true }; };

// Define summation function through operator+ as a template function enabled
// only for the desired types.
template< class T,
class = typename std::enable_if< supported_types< T >::value, T>::type >
NodeIndex operator+( const NodeIndex& ni, T s )
{
  NodeIndex result( ni );
  result.x += s;
  result.y += s;
  result.z += s;

  return result;
}
