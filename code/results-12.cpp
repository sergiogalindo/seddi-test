#include "NodeIndex.h"
#include <vector>




int main ( int argc, char** argv )
{


  // *****************
  //  Question 1 code
  NodeIndex  idx (  10 ,  20 ,  30 );
  std::vector< uint8_t > myVector { 4  ,  5  ,  6  };
  auto value = std::make_pair( idx, std::move(myVector));

  // Check whether values are correct
  value.first.print( );


  // *****************
  //  Question 2 code

  // Values for testing summation function
  uint32_t uint32 = 4;
  int32_t int32 = -4;
  float flo = 3.0f;
  double dou = 7.0;

  // Check uint32_t
  auto node = idx + uint32;
  node.print( );

  // Check int32_t
  node = idx + int32;
  node.print( );

  // Check float
  node = idx + flo;
  node.print( );

  // Check double
  node = idx + dou;
  node.print( );

  // Uncomment and compile to check char
  //node = idx + 'c';
  //node.print( );

	return 0;
}
