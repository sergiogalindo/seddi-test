#include <memory>
#include <iostream>

struct A
{
  A()
  :a( new int( 3 ))
  { }

  //virtual // Uncomment to test
  ~A()
  {
    std::cout << "A destructor" << std::endl;
    delete a;
  };

  int *a;
};

struct B : public A {
  B()
  :b( new int( 4 ))
  {}

  ~B( )
  {
    std::cout << "B destructor" << std::endl;
    delete b;
  }

  int *b;
};

int main ( int argc, char** argv )
{
  std::unique_ptr< A > a = std::make_unique< B >( );

	return 0;
}
