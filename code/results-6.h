#include <vector>

namespace Behavior
{
  class Entity;
  class System;
  class ComponentType;
  class PositionComputer;

  struct CPlaceable : public ComponentType
  {
  public:
    static int type = 0;
  };

  struct CMovable : public ComponentType
  {
  public:
    static int type = 1;
  };


  // This class is in charge of executing the position calculation. By developing
  // different implementations of the calculatePosition method different
  // equations could be applied by exchanging this object.
  class BasicComputer : public PositionComputer
  {
    virtual ~BasicComputer( );
    virtual Vec3 calculatePosition( const Vec3& position,
                                    float velocity,
                                    float dt )
    {
      Vec3 result = position;
      result += velocity * dt;
      return result;
    }
  };

  // This system is responsible for moving all the Movable entities every frame.
  class SystemMovable : public System
  {
  public:

    virtual ~SystemMovable( );

    // This method is executed on every frame to displace all the entities with
    // a Movable component attached to it.
    void Move( void )
    {
      // Get current delta time
      float deltaTime = getDeltaTime( );

      // Accessing all entities with ComponentC and ComponentD
      std::vector< Entity* >& entitiesMovable = getEntities( CMovable::type );

      // This loop could be processed in parallel
      for( auto entity : entitiesMovable )
      {
        _move( entity, deltaTime );
      }
    }

  protected:

    // This method obtains the specific components and updates object's position.
    virtual void _move( Entity* entity, float deltaTime )
    {
      // Get movement-related components
      auto placeableComp = entity->getComponent( CPlaceable::type );
      auto movableComp = entity->getComponent( CMovable::type );

      // Calculate new position
      Vec3 newPos =
          positionComputer.calculatePosition( placeableComp->Position( ),
                                              movableComp->Velocity( ),
                                              deltaTime );
      // Set new position
      placeableComp->setPosition( newPos );
    }

    PositionComputer positionComputer;
  };


}
